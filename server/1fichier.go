package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/parnurzeal/gorequest"
)

// APIKey 1fichier API key
const APIKey string = "dcPi2Tsg4kHq1dApYmQX8mcCdO-sce-z"

// APITokenURL 1fichier API token URL
const APITokenURL = "https://api.1fichier.com/v1/download/get_token.cgi"

// GetFileURL downloads a fil from 1fichier
func GetFileURL(url string, apiKey string) (string, error) {
	log.Println("Request to download: " + string(url))

	request := gorequest.New()
	_, body, errs := request.Post(APITokenURL).
		Set("Authorization", "Bearer "+apiKey).
		Send(fmt.Sprintf(`{"url":"%s", "inline":1}`, url)).
		End()

	if errs != nil {
		return "", fmt.Errorf("Error getting 1fichier token with error: '%s'", errs[0])
	}

	var jsonData map[string]interface{}

	if err := json.Unmarshal([]byte(body), &jsonData); err != nil {
		log.Printf("Unable to parse 1fichier response")
		return "", fmt.Errorf("Unable to parse 1fichier response")
	}

	if jsonData["status"].(string) == "KO" {
		return "", fmt.Errorf("Error received from 1fichier: '%s'", jsonData["message"].(string))
	}

	fileURL := jsonData["url"].(string)

	return fileURL, nil
}
