package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

// WebsocketInMessage struct
type WebsocketInMessage struct {
	Action string      `json:"action"`
	Data   interface{} `json:"data"`
}

// WebsocketOutMessage struct
type WebsocketOutMessage struct {
	Action string      `json:"action"`
	Data   interface{} `json:"data"`
}

// WebSocket struct
type WebSocket struct {
	downloadManager *DownloadManager
	conn            *websocket.Conn
	Reader          chan WebsocketInMessage
}

type updateMessage struct {
	ActiveDownloads []*Download `json:"active_downloads"`
}

// NewWebSocket create a new Websocket
func NewWebSocket(downloadManager *DownloadManager, w http.ResponseWriter, r *http.Request) (*WebSocket, error) {
	// Initiate the WebSocket and allows all origins
	// TODO: Create an environment variable to configure allowed origins
	upgrader := websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool { return true },
	}
	conn, err := upgrader.Upgrade(w, r, nil)

	if err != nil {
		return nil, err
	}

	return &WebSocket{
		downloadManager: downloadManager,
		conn:            conn,
		Reader:          make(chan WebsocketInMessage),
	}, nil
}

// ReadPump starts the read pump to process incoming messages from client
func (w *WebSocket) ReadPump() {
	for {
		_, message, err := w.conn.ReadMessage()
		if err != nil {
			log.Printf("error: %v\n", err)
			break
		}

		var messageJSON WebsocketInMessage
		if err = json.Unmarshal(message, &messageJSON); err != nil {
			log.Println("Error parsing message")
		}

		// Write the received message to the Reader channel
		w.Reader <- messageJSON
	}
}

// Send sends a message to the client
func (w *WebSocket) Send(message WebsocketOutMessage) {
	w.conn.WriteJSON(message)
}
