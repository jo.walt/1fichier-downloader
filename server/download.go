package main

import (
	"context"
	"time"

	"github.com/cavaliercoder/grab"
)

// Download struct
type Download struct {
	Request        *grab.Request      `json:"-"`
	Response       *grab.Response     `json:"-"`
	CancelFunc     context.CancelFunc `json:"-"`
	ID             int64              `json:"id"`
	URL            string             `json:"url"`
	Filename       string             `json:"filename"`
	BytesComplete  int64              `json:"bytesComplete"`
	Size           int64              `json:"size"`
	Progress       float64            `json:"progress"`
	BytesPerSecond float64            `json:"bytesPerSecond"`
	Ellapsed       int64              `json:"ellapsed"`
	ETA            time.Time          `json:"eta"`
	Done           bool               `json:"done"`
	Paused         bool               `json:"paused"`
}

// NewDownload creates a new download with the given URL
func NewDownload(url string) *Download {
	return &Download{
		URL: url,
	}
}

// NewDownloadWithID creates a new download with the given URL and id
func NewDownloadWithID(id int64, url string) *Download {
	return &Download{
		ID:  id,
		URL: url,
	}
}

// Start starts a new download
func (d *Download) Start(url string, directory string, updateInternal int64) {
	ctx, cancel := context.WithCancel(context.Background())
	client := grab.NewClient()
	d.Request, _ = grab.NewRequest(directory, url)
	d.Response = client.Do(d.Request.WithContext(ctx))

	d.CancelFunc = cancel

	// Wait for the download to be finished
	<-d.Response.Done
}

// UpdateProgress updates the progress of the download
func (d *Download) UpdateProgress(isDone bool) {
	if d.Request == nil || d.Response == nil {
		return
	}

	d.URL = d.Request.URL().String()
	d.Filename = d.Response.Filename
	d.BytesComplete = d.Response.BytesComplete()
	d.Size = d.Response.Size
	d.Progress = d.Response.Progress()
	d.BytesPerSecond = d.Response.BytesPerSecond()
	d.Ellapsed = d.Response.Duration().Milliseconds()
	d.ETA = d.Response.ETA()

	if isDone {
		d.Done = !d.Paused
	}
}

// Pause pauses a download
func (d *Download) Pause() {
	d.Paused = true
	d.Cancel()
}

// Cancel cancels a download
func (d *Download) Cancel() {
	d.CancelFunc()
}
