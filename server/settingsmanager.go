package main

import (
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

// Settings struct
type Settings struct {
	apiKey         string
	downloadFolder string
}

// SettingsManager struct
type SettingsManager struct {
	filename string
	//database       *sql.DB
	database *Database
	Settings map[string]string
}

// NewSettingsManager creaates a new SQLite item
func NewSettingsManager(database *Database) *SettingsManager {
	settingsManager := SettingsManager{
		database: database,
		Settings: make(map[string]string),
	}

	return &settingsManager
}

// Get gets a setting value
func (s *SettingsManager) Get(setting string) string {
	return s.Settings[setting]
}

// Set sets a setting value
func (s *SettingsManager) Set(setting string, value string) {
	s.Settings[setting] = value
}

// SaveSettings save settings into database
func (s *SettingsManager) SaveSettings() error {
	saveSettingsSQL := fmt.Sprintf(`
		INSERT INTO settings VALUES ("apiKey", "%[1]v")
			ON CONFLICT (field) DO UPDATE SET value = "%[1]v" WHERE field = "apiKey";
		INSERT INTO settings VALUES ("downloadFolder", "%[2]v")
			ON CONFLICT (field) DO UPDATE SET value = "%[2]v" WHERE field = "downloadFolder";
	`, s.Settings["apiKey"], s.Settings["downloadFolder"])
	_, err := s.database.Db.Exec(saveSettingsSQL)

	return err
}

// LoadSettings load settings from the database
func (s *SettingsManager) LoadSettings() error {
	loadSettingsSQL := `
		SELECT * FROM settings;
	`
	rows, err := s.database.Db.Query(loadSettingsSQL)
	if err != nil {
		return err
	}

	defer rows.Close()
	for rows.Next() {
		var field string
		var value string
		err = rows.Scan(&field, &value)
		if err != nil {
			return err
		}

		s.Settings[field] = value
	}

	return nil
}
