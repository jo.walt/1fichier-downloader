package main

import (
	"fmt"
	"log"
	"os"
)

// DownloadManager struct
type DownloadManager struct {
	activeDownloads map[string]*Download
	directory       string
	updateInternal  int64
	database        *Database
}

// NewDownloadManager creates a new DownloadManager
func NewDownloadManager(directory string, updateInternal int64, database *Database) *DownloadManager {
	dm := DownloadManager{
		activeDownloads: make(map[string]*Download),
		directory:       directory,
		updateInternal:  updateInternal,
		database:        database,
	}

	return &dm
}

// LoadDownloads load the downloads from the database. Should be called at start
func (dm *DownloadManager) LoadDownloads() error {
	// Select all downloads from the database
	rows, err := dm.database.Db.Query("SELECT id, url FROM downloads")
	if err != nil {
		return err
	}

	// Loop through all rows
	defer rows.Close()
	for rows.Next() {
		var id int64
		var url string
		err = rows.Scan(&id, &url)
		if err != nil {
			return err
		}

		// Create a new download and add it the the active downloads list
		d := NewDownloadWithID(id, url)
		d.Paused = true
		dm.activeDownloads[url] = d
	}

	return nil
}

// StartDownload adds and starts a new download to this DownloadManager
func (dm *DownloadManager) StartDownload(url string) {
	// Check if the url is not already in the active download list
	d, exists := dm.activeDownloads[url]

	// Check if a download already exists with this url
	if !exists {
		// No download exists so create a download and add it the active download list
		d = NewDownload(url)
		dm.activeDownloads[url] = d
	} else {
		// A download exists, remove the paused flag
		d.Paused = false
	}

	// Add the download to the database
	if err := dm.addToDatabase(d); err != nil {
		log.Println(err)
		return
	}

	// Start the download and wait for completion
	d.Start(url, dm.directory, dm.updateInternal)

	// Update progress one last time
	d.UpdateProgress(true)
}

// UpdateProgress updates the progress of all downloads
func (dm *DownloadManager) UpdateProgress() {
	for _, d := range dm.activeDownloads {
		d.UpdateProgress(false)
	}
}

// PauseDownload pauses a download
func (dm *DownloadManager) PauseDownload(url string) {
	dm.activeDownloads[url].Pause()
}

// ResumeDownload resumes a download from the active download list
func (dm *DownloadManager) ResumeDownload(url string) {
	dm.StartDownload(url)
}

// CancelDownload cancels a download from the active download list and optionally delete the associated file
func (dm *DownloadManager) CancelDownload(url string, deleteFile bool) {
	d := dm.activeDownloads[url]

	// If the download is not paused, cancel it first
	if !d.Paused {
		d.Cancel()
	}

	// Removes the download from the active download list
	dm.RemoveDownload(url, deleteFile)
}

// RemoveDownload removes a download from the active download list and optionally delete the associated file
func (dm *DownloadManager) RemoveDownload(url string, deleteFile bool) {
	d := dm.activeDownloads[url]

	// Remove the download from the database
	dm.removeFromDatabase(d)

	// Delete the saved file if asked to
	if deleteFile {
		dm.DeleteFile(dm.activeDownloads[url])
	}

	// Remove the download from the active download list
	delete(dm.activeDownloads, url)
}

// DeleteFile removes the file associated to this download
func (dm *DownloadManager) DeleteFile(d *Download) {
	e := os.Remove(d.Filename)
	if e != nil {
		log.Println(e)
	}
}

// addToDatabase adds a download to the database
func (dm *DownloadManager) addToDatabase(d *Download) error {
	log.Println("Add to database ", d.URL)
	// Insert a new row in the downloads table if the download is not already in the database
	res, err := dm.database.Db.Exec(fmt.Sprintf(`
	INSERT OR IGNORE INTO downloads(url, filename, size)
		VALUES ("%v", "%v", "%v")
	`, d.URL, d.Filename, d.Size))
	if err != nil {
		return err
	}

	// Get the last inserted ID
	lastInsertID, err := res.LastInsertId()
	if err != nil {
		return err
	}

	// Set the download ID
	d.ID = lastInsertID

	return nil
}

// removeFromDatabase removes a download from the database
func (dm *DownloadManager) removeFromDatabase(d *Download) error {
	_, err := dm.database.Db.Exec(fmt.Sprintf("DELETE FROM downloads WHERE id=%v", d.ID))

	return err
}

// SaveDownloads save the downloads to the database
func (dm *DownloadManager) SaveDownloads() {
	for _, d := range dm.activeDownloads {
		_, err := dm.database.Db.Exec(fmt.Sprintf(`
		UPDATE downloads
			SET bytesComplete=%v, progress=%v, ellapsed=%v, done=%v, paused=%v
			WHERE id = %v
		`, d.BytesComplete, d.Progress, d.Ellapsed, d.Done, d.Paused, d.ID))
		if err != nil {
			log.Println(err)
		}
	}
}
