import { Writable, writable } from 'svelte/store'
import type { Download } from './download'

export type Settings = {
  apiKey: string
  downloadFolder: string
}

declare interface Collection<T> {
  get(id: string): T | undefined
  edit(key: string, t: T)
  add(t: T): void
  remove(key: string): void
  clear(): void
}

type CollectionStore<T> = Collection<T> & Writable<Map<string, T>>

export const collectionStore = <T,>(uniqueField: string): CollectionStore<T> => {
  let items: Map<string, T> = new Map<string, T>()
  const { subscribe, set, update } = writable(items)

  const get = (id: string) => {
    return items.get(id)
  }

  const add = (t: T) => {
    edit(t[uniqueField], t)
  }

  const edit = (key: string, t: T) => {
    items.set(key, t)
    saveItems()
  }

  const remove = (key: string) => {
    items.delete(key)
    saveItems()
  }

  const clear = () => {
    items.clear()
    saveItems()
  }

  const saveItems = () => {
    set(new Map<string, T>(items))
    localStorage.setItem("downloads", JSON.stringify(items))
  }

  return {
    subscribe,
    set,
    update,
    get,
    edit,
    add,
    remove,
    clear,
  };
}

export const activeDownloadStore = collectionStore<Download>("url")
export const wsMessageStore = writable({})
export const settingsStore = writable({} as Settings)