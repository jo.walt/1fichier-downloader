export class Download {
  url: string
  filename: string
  started: boolean
  startTime: Date
  ended: boolean
  endTime: Date
  ellapsed: number
  size: number
  completed: number
  progress: number
  speed: number
  paused: boolean
  eta: Date
  remaining: number

  constructor(url: string) {
    this.url = url
    this.started = true
    this.startTime = new Date()
    this.completed = 0
    this.progress = 0
    this.speed = 0
  }

  pause() {
    this.paused = true
  }

  resume() {
    this.paused = false
  }

  setProgress(filename: string, size: number, completed: number, progress: number, speed: number, ellasped: number, eta: string, done: boolean, paused: boolean) {
    this.filename = filename
    this.size = size
    this.completed = completed
    this.progress = Math.floor(100 * progress)
    this.speed = speed
    this.ellapsed = ellasped
    this.eta = new Date(eta)
    this.remaining = this.eta.getTime() - new Date().getTime()
    this.paused = paused

    if (done) {
      this.ended = true
      this.endTime = new Date()
    }
  }

  formatSize = () => this.formatBytes(this.size)

  formatCompleted = () => this.formatBytes(this.completed)

  formatProgress = () => (this.progress * 100) + " %"

  formatSpeed = () => this.formatBytes(this.speed) + "/s"

  formatBytes(bytes: number): string {
    if (isNaN(bytes)) {
      return ''
    }

    if (bytes < 1024)
      return `${bytes} B`

    if (bytes < 1048576)
      return `${(bytes / 1024).toFixed(2)} KB`

    if (bytes < 1073741824)
      return `${(bytes / 1048576).toFixed(2)} MB`

    if (bytes < 1099511627776)
      return `${(bytes / 1073741824).toFixed(2)} GB`
  }

  formatRemaining() {
    return this.remaining > 0 ? this.formatTime(this.remaining) + " remaining" : ""
  }

  formatTime(time: number) {
    let seconds = Math.floor(time / 1000);
    let minutes = Math.floor(seconds / 60); seconds = seconds % 60;
    let hours = Math.floor(minutes / 60); minutes = minutes % 60;
    let days = Math.floor(hours / 24); hours = hours % 24;

    let result = []

    if (days > 0) result.push(`${days} d`)
    if (hours > 0) result.push(`${hours} h`)
    if (minutes > 0) result.push(`${minutes} m`)
    if (seconds > 0) result.push(`${seconds} s`)

    return result.join(" ")
  }

  formatEta() {
    return this.eta ? (this.eta.toLocaleDateString() + " " + this.eta.toLocaleTimeString()) : 'unknown'
  }

  toString() {
    return this.url
  }
}